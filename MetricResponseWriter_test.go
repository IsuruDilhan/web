package web

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type MetricResponseWriterSuite struct {
	suite.Suite

	w *httptest.ResponseRecorder

	x *MetricResponseWriter
}

func TestMetricResponseWriterSuite(t *testing.T) {
	suite.Run(t, &MetricResponseWriterSuite{})
}

func (suite *MetricResponseWriterSuite) SetupTest() {
	suite.w = httptest.NewRecorder()

	suite.x = NewMetricResponseWriter(suite.w)
}

func (suite *MetricResponseWriterSuite) TestShouldGetCorrectHeaders() {
	// Arrange.
	suite.w.Header().Set("X-Test-Header", "test-value")

	// Act.
	headers := suite.x.Header()
	testHeaderValue := headers.Get("X-Test-Header")

	// Assert.
	suite.Equal(suite.w.Header(), headers)
	suite.Equal(suite.w.Header().Get("X-Test-Header"), testHeaderValue)
}

func (suite *MetricResponseWriterSuite) TestShouldWriteAndRecordVolumeCorrectly() {
	// Arrange.
	suite.x.Write([]byte("Hello, World!"))

	// Act.
	volume := suite.x.Volume()
	response := suite.w.Result()
	raw, err := ioutil.ReadAll(response.Body)

	// Assert.
	suite.Nil(err)
	suite.Equal("Hello, World!", string(raw))
	suite.Equal(uint64(13), volume)
}

func (suite *MetricResponseWriterSuite) TestShouldOnlySetResponseCodeOnce() {
	// Arrange.
	suite.x.WriteHeader(http.StatusBadRequest)
	suite.x.WriteHeader(http.StatusForbidden)

	// Act.
	response := suite.w.Result()

	// Assert.
	suite.Equal(http.StatusBadRequest, response.StatusCode)
	suite.Equal(http.StatusBadRequest, suite.x.StatusCode())
}

func (suite *MetricResponseWriterSuite) TestShouldReturn200ByDefault() {
	// Arrange, Act and Assert.
	suite.Equal(http.StatusOK, suite.x.StatusCode())
}

func (suite *MetricResponseWriterSuite) TestShouldReturnFalseForHasWrittenHeaders() {
	// Arrange and Act.
	hasWrittenHeaders := suite.x.HasWrittenHeaders()

	// Assert.
	suite.False(hasWrittenHeaders)
}

func (suite *MetricResponseWriterSuite) TestShouldReturnTrueForHasWrittenHeaders() {
	// Arrange.
	suite.x.WriteHeader(http.StatusCreated)

	// Act.
	hasWrittenHeaders := suite.x.HasWrittenHeaders()

	// Assert.
	suite.True(hasWrittenHeaders)
}

func (suite *MetricResponseWriterSuite) TestShouldReturnCorrectDuration() {
	// Arrange.
	time.Sleep(time.Millisecond * 50)

	// Act.
	dur := suite.x.Duration()

	// Assert.
	expected := float64(time.Millisecond) * 50
	actual := float64(dur)
	delta := float64(time.Millisecond) * 5
	suite.InDelta(expected, actual, delta)
}
