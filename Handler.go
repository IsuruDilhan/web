package web

import (
	"net/http"

	"gitlab.com/ljpgo/di"
)

// Handler defines a function that can handle a request.  All routes implement a
// Handle method that fits this definition.
type Handler func(c *di.Container, w http.ResponseWriter, r *http.Request, v *RequestVariables) error
