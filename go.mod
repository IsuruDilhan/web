module gitlab.com/ljpgo/web

go 1.12

require (
	github.com/fatih/color v1.7.0
	github.com/gorilla/mux v1.7.3
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/stretchr/testify v1.3.0
	gitlab.com/ljpgo/di v1.0.1
)
