package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"github.com/fatih/color"
)

// These colors are used to add some visual cues to logging output.
var (
	colorWhite  = color.New(color.FgWhite)
	colorGreen  = color.New(color.FgGreen)
	colorYellow = color.New(color.FgYellow)
	colorRed    = color.New(color.FgRed)
	colorBlue   = color.New(color.FgCyan)
)

var rawDefaultErrorResponse = []byte(`{"error":{"type":"HTTP:InternalServerError","message":"a generic server error prevented the operation from completing successfully"}}`)

// RespondWithJSON responds to the specified response writer with a JSON object
// and status code.  If the object cannot be serialized, the server will respond
// with an internal server error.
func RespondWithJSON(w http.ResponseWriter, statusCode int, model interface{}) {
	raw, err := json.Marshal(model)
	if err != nil {
		statusCode = http.StatusInternalServerError
		raw = rawDefaultErrorResponse
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", fmt.Sprintf("%v", len(raw)))
	w.WriteHeader(statusCode)
	w.Write(raw)
}

// ErrorResponse builds an ErrorResponseModel and uses it as the response model
// with the specified status code.
func ErrorResponse(w http.ResponseWriter, statusCode int, errorType string, message string, formatters ...interface{}) {
	ErrorResponseWithSpecifics(w, statusCode, errorType, nil, message, formatters...)
}

// ErrorResponseWithSpecifics builds an ErrorResponseModel, including the given
// specifics, and uses it as the response model with the specified status code.
func ErrorResponseWithSpecifics(w http.ResponseWriter, statusCode int, errorType string, specifics interface{}, message string, formatters ...interface{}) {
	model := &ErrorResponseModel{
		Error: ErrorDetails{
			ErrorType: errorType,
			Message:   fmt.Sprintf(message, formatters...),
			Specifics: specifics,
		},
	}

	RespondWithJSON(w, statusCode, model)
}

// ReadRequestJSON reads the request body into the provided purifiable model.
func ReadRequestJSON(w http.ResponseWriter, r *http.Request, model Purifiable) bool {
	contentType := r.Header.Get("Content-Type")
	if !strings.HasPrefix(r.Header.Get("Content-Type"), "application/json") {
		ErrorResponse(w, http.StatusUnsupportedMediaType, errorTypeHTTPUnsupportedMediaType, "this endpoint expects 'application/json', but a Content-Type of '%v' was provided", contentType)
		return false
	}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(model)
	if err != nil {
		ErrorResponse(w, http.StatusBadRequest, errorTypeHTTPBadRequest, "the provided JSON had an unexpected structure or was otherwise invalid")
		return false
	}

	err = model.Purify()
	if err != nil {
		ErrorResponse(w, http.StatusUnprocessableEntity, errorTypeHTTPUnprocessableEntity, err.Error())
		return false
	}

	return true
}

// TestWithJSON builds a test http.ResponseWriter and *http.Request for the
// provided route, request model and request variables.
func TestWithJSON(route Route, reqModel interface{}, v *RequestVariables) (*httptest.ResponseRecorder, *http.Request) {
	var buf *bytes.Buffer

	raw, err := json.Marshal(reqModel)
	if err == nil {
		buf = bytes.NewBuffer(raw)
	}

	w, r := TestWithBody(route, buf, v)
	r.Header.Set("Content-Type", "application/json")

	return w, r
}

// TestWithBody builds a test http.ResponseWrtier and *http.Request for the
// provided route, body and request variables.  It is valid to pass body as nil.
func TestWithBody(route Route, body io.Reader, v *RequestVariables) (*httptest.ResponseRecorder, *http.Request) {
	method := route.Method()
	path := route.Path()

	w := httptest.NewRecorder()
	r := httptest.NewRequest(method, path, body)
	r = setPathVariables(r, v)

	return w, r
}

// GetModelFromResponseRecorder reads a JSON model from a response recorder.
func GetModelFromResponseRecorder(w *httptest.ResponseRecorder, model interface{}) {
	json.NewDecoder(w.Result().Body).Decode(model)
}

func setPathVariables(r *http.Request, v *RequestVariables) *http.Request {
	return mux.SetURLVars(r, v.PathVariables)
}

func logRequest(mrw *MetricResponseWriter, r *http.Request) {
	method := fmt.Sprintf("%-7v", r.Method)
	statusCode := mrw.StatusCode()
	duration := formatDuration(mrw.Duration())
	volume := formatByteVolume(mrw.Volume())
	colorStatusCode := getStatusCodeColor(statusCode)

	colorStatusCode.Printf("• %v", statusCode)
	colorWhite.Printf(" %v %v ", duration, volume)
	colorWhite.Printf("%v ", method)
	colorBlue.Printf("%v\n", r.URL.Path)
}

func logError(err interface{}) {
	if err == nil {
		return
	}

	colorRed.Printf("      %v\n", err)
}

func formatByteVolume(volume uint64) string {
	suffixes := []string{" B", "kB", "MB", "GB", "TB"}

	vol := float64(volume)
	ind := 0

	for vol >= 1024 {
		vol /= 1024

		if ind != len(suffixes)-1 {
			ind++
		}
	}

	return fmt.Sprintf("%7.2f %v", vol, suffixes[ind])
}

func formatDuration(duration time.Duration) string {
	if duration < time.Microsecond {
		return "    <1 µs"
	}

	suffixes := []string{"ns", "µs", "ms", " s"}

	dur := float64(duration)
	ind := 0

	for dur >= 1000 {
		dur /= 1000

		if ind != len(suffixes)-1 {
			ind++
		}
	}

	return fmt.Sprintf("%6.2f %v", dur, suffixes[ind])
}

func getStatusCodeColor(statusCode int) *color.Color {
	colorStatusCode := colorWhite

	if statusCode >= 200 {
		colorStatusCode = colorGreen
	}
	if statusCode >= 300 {
		colorStatusCode = colorWhite
	}
	if statusCode >= 400 {
		colorStatusCode = colorYellow
	}
	if statusCode >= 500 {
		colorStatusCode = colorRed
	}

	return colorStatusCode
}
