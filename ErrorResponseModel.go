package web

// Empty commit
// ErrorResponseModel defines a generic error structure to be returned in the
// event of an error.
type ErrorResponseModel struct {
	Error ErrorDetails `json:"error"`
}

// ErrorDetails is the internal error details of an ErrorResponseModel.
type ErrorDetails struct {
	ErrorType string      `json:"type"`
	Message   string      `json:"message"`
	Specifics interface{} `json:"specifics,omitempty"`
}
