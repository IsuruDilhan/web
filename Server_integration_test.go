package web

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/ljpgo/di"
)

type ServerIntegrationSuite struct {
	suite.Suite

	x *Server
}

func TestServerIntegrationSuite(t *testing.T) {
	suite.Run(t, &ServerIntegrationSuite{})
}

func (suite *ServerIntegrationSuite) SetupTest() {
	suite.x = NewServer(8080, di.New())
	suite.x.DebuggingEnvironment = true
}

func (suite *ServerIntegrationSuite) TearDownTest() {
	suite.x.Stop()
}

func (suite *ServerIntegrationSuite) TestShouldNotAllowAddingMiddlewareAfterRoutes() {
	// Arrange.
	suite.x.Use(&testRoute{})

	// Act and Assert.
	suite.Panics(func() {
		suite.x.AddDefaultMiddleware(&testMiddleware{}, false)
	})
}

func (suite *ServerIntegrationSuite) TestShouldHandleTwoDifferentMethodsOnSamePath() {
	// Arrange.
	suite.x.Use(&testRoute{method: http.MethodGet, path: "/", middleware: nil})
	suite.x.Use(&testRoute{method: http.MethodPost, path: "/", middleware: nil})

	// Act.
	go func() {
		suite.x.Start()
	}()

	res1, err := http.Get("http://localhost:8080")
	suite.Nil(err)
	defer res1.Body.Close()

	res2, err := http.Post("http://localhost:8080", "", nil)
	suite.Nil(err)
	defer res2.Body.Close()

	raw1, err := ioutil.ReadAll(res1.Body)
	suite.Nil(err)

	raw2, err := ioutil.ReadAll(res2.Body)
	suite.Nil(err)

	// Assert.
	suite.Equal(http.StatusOK, res1.StatusCode)
	suite.Equal(http.StatusOK, res2.StatusCode)
	suite.Equal("GET / ", string(raw1))
	suite.Equal("POST / ", string(raw2))
}

func (suite *ServerIntegrationSuite) TestShouldUseMiddlewareProperly() {
	// Arrange.
	suite.x.AddDefaultMiddleware(&testMiddleware{key: "m1", value: "1"}, false)
	suite.x.AddDefaultMiddleware(&testMiddleware{key: "m2", value: "2"}, true)
	suite.x.Use(&testRoute{method: http.MethodGet, path: "/test", middleware: []Middleware{
		&testMiddleware{key: "m3", value: "3"},
	}})

	// Act.
	go func() {
		suite.x.Start()
	}()

	res, err := http.Get("http://localhost:8080/test")
	suite.Nil(err)
	defer res.Body.Close()

	raw, err := ioutil.ReadAll(res.Body)
	suite.Nil(err)

	// Assert.
	suite.Equal(http.StatusOK, res.StatusCode)
	suite.Equal("GET /test m1:1 m2:2 m3:3 ", string(raw))
}

func (suite *ServerIntegrationSuite) TestShouldCorrectlyRespondWithNotFound() {
	// Arrange and Act.
	go func() {
		suite.x.Start()
	}()

	res, err := http.Get("http://localhost:8080/test")
	suite.Nil(err)
	defer res.Body.Close()

	raw, err := ioutil.ReadAll(res.Body)
	suite.Nil(err)

	// Assert.
	suite.Equal(http.StatusNotFound, res.StatusCode)
	suite.Equal(`{"error":{"type":"HTTP:NotFound","message":"the requested resource was not found"}}`, string(raw))
}

func (suite *ServerIntegrationSuite) TestShouldHandleBubbledError() {
	// Arrange.
	suite.x.Use(&testRoute{method: http.MethodGet, path: "/error", returnsError: true})

	// Act.
	go func() {
		suite.x.Start()
	}()

	res, err := http.Get("http://localhost:8080/error")
	suite.Nil(err)
	defer res.Body.Close()

	raw, err := ioutil.ReadAll(res.Body)
	suite.Nil(err)

	// Assert.
	suite.Equal(http.StatusInternalServerError, res.StatusCode)
	suite.Equal(`{"error":{"type":"HTTP:InternalServerError","message":"an unhandled error occurred"}}`, string(raw))
}

func (suite *ServerIntegrationSuite) TestShouldHandlePanic() {
	// Arrange.
	suite.x.Use(&testRoute{method: http.MethodGet, path: "/panic", panics: true})

	// Act.
	go func() {
		suite.x.Start()
	}()

	res, err := http.Get("http://localhost:8080/panic")
	suite.Nil(err)
	defer res.Body.Close()

	raw, err := ioutil.ReadAll(res.Body)
	suite.Nil(err)

	// Assert.
	suite.Equal(http.StatusInternalServerError, res.StatusCode)
	suite.Equal(`{"error":{"type":"HTTP:InternalServerError","message":"i'm panicking in testRoute"}}`, string(raw))
}

func (suite *ServerIntegrationSuite) TestShouldHandleMiddlewareError() {
	// Arrange.
	suite.x.AddDefaultMiddleware(&testMiddleware{returnsError: true}, false)
	suite.x.Use(&testRoute{method: http.MethodGet, path: "/test", panics: true})

	// Act.
	go func() {
		suite.x.Start()
	}()

	res, err := http.Get("http://localhost:8080/test")
	suite.Nil(err)
	defer res.Body.Close()

	raw, err := ioutil.ReadAll(res.Body)
	suite.Nil(err)

	// Assert.
	suite.Equal(http.StatusInternalServerError, res.StatusCode)
	suite.Equal(`{"error":{"type":"HTTP:InternalServerError","message":"an unhandled middleware error occurred"}}`, string(raw))
}

func (suite *ServerIntegrationSuite) TestShouldNotRunHandlerIfMiddlewareFails() {
	// Arrange.
	suite.x.AddDefaultMiddleware(&testMiddleware{fails: true}, false)
	suite.x.Use(&testRoute{method: http.MethodGet, path: "/test", panics: true})

	// Act.
	go func() {
		suite.x.Start()
	}()

	res, err := http.Get("http://localhost:8080/test")
	suite.Nil(err)
	defer res.Body.Close()

	// Assert.
	suite.Equal(http.StatusConflict, res.StatusCode)
}

func (suite *ServerIntegrationSuite) TestShouldNotRunNotFoundIfMiddlewareFails() {
	// Arrange.
	suite.x.AddDefaultMiddleware(&testMiddleware{fails: true}, true)

	// Act.
	go func() {
		suite.x.Start()
	}()

	res, err := http.Get("http://localhost:8080/test")
	suite.Nil(err)
	defer res.Body.Close()

	// Assert.
	suite.Equal(http.StatusConflict, res.StatusCode)
}

// -----------------------------------------------------------------------------

type testRoute struct {
	method       string
	path         string
	middleware   []Middleware
	returnsError bool
	panics       bool
}

func (tr *testRoute) Method() string {
	return tr.method
}

func (tr *testRoute) Path() string {
	return tr.path
}

func (tr *testRoute) Middleware() []Middleware {
	return tr.middleware
}

func (tr *testRoute) Handle(c *di.Container, w http.ResponseWriter, r *http.Request, v *RequestVariables) error {
	if tr.returnsError {
		return fmt.Errorf("an unhandled error occurred")
	}

	if tr.panics {
		panic("i'm panicking in testRoute")
	}

	method := r.Method
	path := r.URL.Path

	fmt.Fprintf(w, "%v %v ", method, path)

	keys := make([]string, 0)
	for k := range v.MiddlewareArtifacts {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		fmt.Fprintf(w, "%v:%v ", k, v.MiddlewareArtifacts[k])
	}

	return nil
}

type testMiddleware struct {
	key          string
	value        string
	returnsError bool
	panics       bool
	fails        bool
}

func (tm *testMiddleware) Review(w http.ResponseWriter, r *http.Request, ma map[string]interface{}) (bool, error) {
	if tm.returnsError {
		return false, fmt.Errorf("an unhandled middleware error occurred")
	}

	if tm.panics {
		panic("i'm panicking in testMiddleware")
	}

	if tm.fails {
		w.WriteHeader(http.StatusConflict)
		return false, nil
	}

	ma[tm.key] = tm.value
	return true, nil
}
