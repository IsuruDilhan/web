package web

import (
	"net/http"

	"gitlab.com/ljpgo/di"
)

// Route describes a type that serves requests for a provided route and method.
type Route interface {
	Method() string
	Path() string
	Middleware() []Middleware

	Handle(c *di.Container, w http.ResponseWriter, r *http.Request, v *RequestVariables) error
}
