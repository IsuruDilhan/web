package web

import (
	"net/http"

	"github.com/gorilla/mux"
)

// RequestVariables contains various sources of data available for use in
// request handlers.
type RequestVariables struct {
	MiddlewareArtifacts map[string]interface{}
	PathVariables       map[string]string
}

func createRequestVariables(r *http.Request, ma map[string]interface{}) *RequestVariables {
	return &RequestVariables{
		MiddlewareArtifacts: ma,
		PathVariables:       mux.Vars(r),
	}
}
