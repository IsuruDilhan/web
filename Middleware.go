package web

import "net/http"

// Middleware describes a type that intercepts incoming HTTP requests.  It
// returns false to indicate that the request failed the conditions required by
// the middleware.  It can freely modify the provided
// MiddlewareArtifactCollection that is eventually passed to the Route.
type Middleware interface {
	Review(w http.ResponseWriter, r *http.Request, ma map[string]interface{}) (bool, error)
}
