package web

import (
	"net/http"
	"time"
)

// MetricResponseWriter wraps a standard http.ResponseWriter with additional
// functionality.  Specifically, it records the amount of data written, the
// response code, and the duration of the request/response.
type MetricResponseWriter struct {
	w                 http.ResponseWriter
	startTime         time.Time
	statusCode        int
	volume            uint64
	hasWrittenHeaders bool
}

// NewMetricResponseWriter creates a new MetricResponseWriter with the provided
// underlying http.ResponseWriter.
func NewMetricResponseWriter(w http.ResponseWriter) *MetricResponseWriter {
	return &MetricResponseWriter{
		w:         w,
		startTime: time.Now(),
	}
}

// Header simply returns the headers of the underlying response writer.
func (mrw *MetricResponseWriter) Header() http.Header {
	return mrw.w.Header()
}

// Write writes to the underlying response writer, recording the number of bytes
// successfully written.
func (mrw *MetricResponseWriter) Write(b []byte) (int, error) {
	n, err := mrw.w.Write(b)
	mrw.volume += uint64(n)

	return n, err
}

// WriteHeader records and writes the header if it has not already been written.
func (mrw *MetricResponseWriter) WriteHeader(statusCode int) {
	if mrw.hasWrittenHeaders {
		return
	}

	mrw.statusCode = statusCode
	mrw.w.WriteHeader(statusCode)
	mrw.hasWrittenHeaders = true
}

// StatusCode returns the status code that was written for the response.  If the
// status code is yet to be written, or WriteHeader was never explicitly called,
// StatusCode will return http.StatusOK.
func (mrw *MetricResponseWriter) StatusCode() int {
	if mrw.statusCode == 0 {
		return http.StatusOK
	}

	return mrw.statusCode
}

// HasWrittenHeaders returns true if WriteHeader has been called.
func (mrw *MetricResponseWriter) HasWrittenHeaders() bool {
	return mrw.hasWrittenHeaders
}

// Duration returns the duration between the start of the request and now.
func (mrw *MetricResponseWriter) Duration() time.Duration {
	return time.Now().Sub(mrw.startTime)
}

// Volume returns the number of bytes written to the response writer body.
func (mrw *MetricResponseWriter) Volume() uint64 {
	return mrw.volume
}
