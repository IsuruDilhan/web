package web

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/ljpgo/di"
)

// Server is the main type of this package.  It exposes the provided routes over
// HTTP on the given port whilst managing things like middleware, logging and
// error handling.  When DebuggingEnvironment is true, the server will pass
// errors back in HTTP responses.
type Server struct {
	port               uint16
	mx                 *mux.Router
	defaultMiddleware  map[Middleware]bool
	hasRegisteredRoute bool
	svr                *http.Server
	container          *di.Container

	DebuggingEnvironment bool
}

// NewServer creates a new server instance on the specified Port.
func NewServer(port uint16, container *di.Container) *Server {
	return &Server{
		port:               port,
		mx:                 mux.NewRouter(),
		defaultMiddleware:  make(map[Middleware]bool),
		hasRegisteredRoute: false,
		container:          container,
	}
}

// AddDefaultMiddleware adds a middleware that will be used for all routes, and
// optionally for the global "Not Found" handler.
func (s *Server) AddDefaultMiddleware(mw Middleware, useOnNotFound bool) {
	if s.hasRegisteredRoute {
		panic("cannot add middleware after registering a route")
	}

	s.defaultMiddleware[mw] = useOnNotFound
}

// Use adds a route to the server.
func (s *Server) Use(route Route) {
	s.hasRegisteredRoute = true

	method := route.Method()
	path := route.Path()
	mws := route.Middleware()

	s.mx.HandleFunc(path, s.buildHandler(mws, route.Handle)).Methods(method)
}

// Start starts the server.  This is a blocking call.
func (s *Server) Start() error {
	mws := s.getDefaultMiddlewareAsSlice(true)

	s.mx.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ma := make(map[string]interface{})
		mrw := NewMetricResponseWriter(w)

		if !s.applyMiddleware(mws, mrw, r, ma) {
			logRequest(mrw, r)
			return
		}

		ErrorResponse(mrw, http.StatusNotFound, errorTypeHTTPNotFound, "the requested resource was not found")
		logRequest(mrw, r)
	})

	s.svr = &http.Server{
		Addr:              fmt.Sprintf(":%v", s.port),
		Handler:           s.mx,
		MaxHeaderBytes:    1 << 19,
		ReadHeaderTimeout: time.Second * 5,
		ReadTimeout:       time.Minute * 5,
		WriteTimeout:      time.Second * 10,
		IdleTimeout:       120 * time.Second,
	}

	return s.svr.ListenAndServe()
}

// Stop stops the server.
func (s *Server) Stop() {
	if s.svr == nil {
		return
	}

	s.svr.Shutdown(context.Background())
}

func (s *Server) buildHandler(mws []Middleware, handler Handler) http.HandlerFunc {
	mws = append(s.getDefaultMiddlewareAsSlice(false), mws...)

	return func(w http.ResponseWriter, r *http.Request) {
		ma := make(map[string]interface{})
		mrw := NewMetricResponseWriter(w)

		defer func() {
			if rec := recover(); rec != nil {
				s.handleBubbledError(mrw, r, rec)
			}
		}()

		if !s.applyMiddleware(mws, mrw, r, ma) {
			logRequest(mrw, r)
			return
		}

		v := createRequestVariables(r, ma)
		err := handler(s.container, mrw, r, v)
		if err != nil {
			s.handleBubbledError(mrw, r, err)
			return
		}

		logRequest(mrw, r)
	}
}

func (s *Server) applyMiddleware(mws []Middleware, mrw *MetricResponseWriter, r *http.Request, ma map[string]interface{}) bool {
	for _, mw := range mws {
		passed, err := mw.Review(mrw, r, ma)

		if err != nil {
			s.handleBubbledError(mrw, r, err)
			return false
		}

		if !passed {
			return false
		}
	}

	return true
}

func (s *Server) handleBubbledError(mrw *MetricResponseWriter, r *http.Request, err interface{}) {
	if !mrw.HasWrittenHeaders() {
		msg := "a generic server error prevented the operation from completing successfully"
		if s.DebuggingEnvironment {
			msg = fmt.Sprintf("%v", err)
		}

		ErrorResponse(mrw, http.StatusInternalServerError, errorTypeHTTPInternalServerError, msg)
	}

	logRequest(mrw, r)
	logError(err)
}

func (s *Server) getDefaultMiddlewareAsSlice(forNotFound bool) []Middleware {
	r := make([]Middleware, 0)

	for mw, useOnNotFound := range s.defaultMiddleware {
		if forNotFound && !useOnNotFound {
			continue
		}

		r = append(r, mw)
	}

	return r
}
