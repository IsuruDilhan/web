package web

// Purifiable defines a set of methods implemented by types that can be
// "purified".  Incoming request models generally implement this interface to
// ensure that potentially unsafe data is filtered and handled properly.
type Purifiable interface {
	Purify() error
}
