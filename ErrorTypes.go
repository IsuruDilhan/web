package web

// A collection of ErrorTypes that are used by this package.
const (
	errorTypeHTTPBadRequest           = "HTTP:BadRequest"
	errorTypeHTTPInternalServerError  = "HTTP:InternalServerError"
	errorTypeHTTPNotFound             = "HTTP:NotFound"
	errorTypeHTTPUnprocessableEntity  = "HTTP:UnprocessableEntity"
	errorTypeHTTPUnsupportedMediaType = "HTTP:UnsupportedMediaType"
)
