package web

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/fatih/color"
	"github.com/stretchr/testify/assert"
)

type alwaysFailsMarshalling struct{}

func (afm *alwaysFailsMarshalling) MarshalJSON() ([]byte, error) {
	return nil, errors.New("marshal failed")
}

func TestRespondWithJSONError(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responseJSON := `{"error":{"type":"HTTP:InternalServerError","message":"a generic server error prevented the operation from completing successfully"}}`
	responseModel := &alwaysFailsMarshalling{}

	// Act.
	RespondWithJSON(w, http.StatusCreated, responseModel)
	response := w.Result()
	raw, err := ioutil.ReadAll(response.Body)

	// Assert.
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, response.StatusCode)
	assert.Equal(t, "application/json", response.Header.Get("Content-Type"))
	assert.Equal(t, fmt.Sprintf("%v", len(responseJSON)), response.Header.Get("Content-Length"))
	assert.Equal(t, responseJSON, string(raw))
}

func TestRespondWithJSONSuccess(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responseJSON := `{"val1":"Hello, World!","val2":5}`
	responseModel := struct {
		Value1 string `json:"val1"`
		Value2 int    `json:"val2"`
	}{
		Value1: "Hello, World!",
		Value2: 5,
	}

	// Act.
	RespondWithJSON(w, http.StatusCreated, responseModel)
	response := w.Result()
	raw, err := ioutil.ReadAll(response.Body)

	// Assert.
	assert.Nil(t, err)
	assert.Equal(t, http.StatusCreated, response.StatusCode)
	assert.Equal(t, "application/json", response.Header.Get("Content-Type"))
	assert.Equal(t, fmt.Sprintf("%v", len(responseJSON)), response.Header.Get("Content-Length"))
	assert.Equal(t, responseJSON, string(raw))
}

func TestErrorResponse(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	responseJSON := `{"error":{"type":"Test:Error","message":"invalid username or password"}}`

	// Act.
	ErrorResponse(w, http.StatusUnauthorized, "Test:Error", "invalid username or password")
	response := w.Result()
	raw, err := ioutil.ReadAll(response.Body)

	// Assert.
	assert.Nil(t, err)
	assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	assert.Equal(t, "application/json", response.Header.Get("Content-Type"))
	assert.Equal(t, fmt.Sprintf("%v", len(responseJSON)), response.Header.Get("Content-Length"))
	assert.Equal(t, responseJSON, string(raw))
}

func TestReadRequestJSONBadContentType(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte{1, 2, 3, 4}))
	responseJSON := `{"error":{"type":"HTTP:UnsupportedMediaType","message":"this endpoint expects 'application/json', but a Content-Type of 'image/png' was provided"}}`

	// Act.
	r.Header.Set("Content-Type", "image/png")
	model := &testPurifiable{shouldThrowError: false}
	ReadRequestJSON(w, r, model)
	response := w.Result()
	raw, err := ioutil.ReadAll(response.Body)

	// Assert.
	assert.Nil(t, err)
	assert.Equal(t, http.StatusUnsupportedMediaType, response.StatusCode)
	assert.Equal(t, "application/json", response.Header.Get("Content-Type"))
	assert.Equal(t, fmt.Sprintf("%v", len(responseJSON)), response.Header.Get("Content-Length"))
	assert.Equal(t, responseJSON, string(raw))
}

func TestReadRequestJSONInvalidJSON(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte{1, 2, 3, 4}))
	responseJSON := `{"error":{"type":"HTTP:BadRequest","message":"the provided JSON had an unexpected structure or was otherwise invalid"}}`

	// Act.
	r.Header.Set("Content-Type", "application/json")
	model := &testPurifiable{shouldThrowError: false}
	ReadRequestJSON(w, r, model)
	response := w.Result()
	raw, err := ioutil.ReadAll(response.Body)

	// Assert.
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.Equal(t, "application/json", response.Header.Get("Content-Type"))
	assert.Equal(t, fmt.Sprintf("%v", len(responseJSON)), response.Header.Get("Content-Length"))
	assert.Equal(t, responseJSON, string(raw))
}

func TestReadRequestJSONFailsPurify(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	responseJSON := `{"error":{"type":"HTTP:UnprocessableEntity","message":"could not purify"}}`

	// Act.
	r.Header.Set("Content-Type", "application/json")
	model := &testPurifiable{shouldThrowError: true}
	ReadRequestJSON(w, r, model)
	response := w.Result()
	raw, err := ioutil.ReadAll(response.Body)

	// Assert.
	assert.Nil(t, err)
	assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
	assert.Equal(t, "application/json", response.Header.Get("Content-Type"))
	assert.Equal(t, fmt.Sprintf("%v", len(responseJSON)), response.Header.Get("Content-Length"))
	assert.Equal(t, responseJSON, string(raw))
}

func TestReadRequestJSONSuccess(t *testing.T) {
	// Arrange.
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte(`{"value":"1234"}`)))

	// Act.
	r.Header.Set("Content-Type", "application/json")
	model := &testPurifiable{shouldThrowError: false}
	ReadRequestJSON(w, r, model)
	response := w.Result()

	// Assert.
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func TestFormatByteVolume(t *testing.T) {
	testCases := []struct {
		given  uint64
		expect string
	}{
		{given: 0, expect: "   0.00  B"},
		{given: 1, expect: "   1.00  B"},
		{given: 999, expect: " 999.00  B"},
		{given: 1023, expect: "1023.00  B"},
		{given: 1024, expect: "   1.00 kB"},
		{given: 1048540, expect: "1023.96 kB"},
		{given: 1048576, expect: "   1.00 MB"},
	}

	for _, testCase := range testCases {
		actual := formatByteVolume(testCase.given)
		assert.Equal(t, testCase.expect, actual)
	}
}

func TestFormatDuration(t *testing.T) {
	testCases := []struct {
		given  int64
		expect string
	}{
		{given: 0, expect: "    <1 µs"},
		{given: 500, expect: "    <1 µs"},
		{given: 1000, expect: "  1.00 µs"},
		{given: 1500, expect: "  1.50 µs"},
		{given: 1560, expect: "  1.56 µs"},
		{given: 1575, expect: "  1.57 µs"},
		{given: 1000000, expect: "  1.00 ms"},
		{given: 1500000, expect: "  1.50 ms"},
		{given: 1500000000, expect: "  1.50  s"},
	}

	for _, testCase := range testCases {
		actual := formatDuration(time.Duration(testCase.given))
		assert.Equal(t, testCase.expect, actual)
	}
}

func TestGetStatusCodeColor(t *testing.T) {
	testCases := []struct {
		given  int
		expect *color.Color
	}{
		{given: 0, expect: colorWhite},
		{given: 50, expect: colorWhite},
		{given: 199, expect: colorWhite},
		{given: 200, expect: colorGreen},
		{given: 250, expect: colorGreen},
		{given: 299, expect: colorGreen},
		{given: 300, expect: colorWhite},
		{given: 350, expect: colorWhite},
		{given: 399, expect: colorWhite},
		{given: 400, expect: colorYellow},
		{given: 450, expect: colorYellow},
		{given: 499, expect: colorYellow},
		{given: 500, expect: colorRed},
		{given: 550, expect: colorRed},
		{given: 599, expect: colorRed},
		{given: 600, expect: colorRed},
	}

	for _, testCase := range testCases {
		actual := getStatusCodeColor(testCase.given)
		assert.Equal(t, testCase.expect, actual)
	}
}

// -----------------------------------------------------------------------------

type testPurifiable struct {
	shouldThrowError bool
	Value            string `json:"value"`
}

func (tp *testPurifiable) Purify() error {
	if tp.shouldThrowError {
		return fmt.Errorf("could not purify")
	}

	return nil
}
